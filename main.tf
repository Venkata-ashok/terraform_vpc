provider "aws" {
  region  = "us-east-1"
}


// Create the VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags= {
    Name = "${var.name}-${var.environment}-vpc"
    environment =  var.environment
  }

}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

// Create the IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags= { 
  	Name = "${var.name}-${var.environment}-igw"
  }
  
}

// Create Public Subnets
resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.vpc.id
  count                   = length(split(",", var.public_subnets_cidr))
  cidr_block              = element(split(",", var.public_subnets_cidr), count.index)
  availability_zone       = element(split(",", var.azs), count.index)
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags= {
    Name = "${var.name}-${var.environment}-public-${element(split(",", var.azs), count.index)}"
  }
}

output "public_subnets_id" {
  value = join(",", aws_subnet.public.*.id)
}

// Create Public Route Table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags= {
    Name = "${var.name}-public"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(split(",", var.public_subnets_cidr))
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

// Create the Private Subnets
resource "aws_subnet" "private" {
  vpc_id                  = aws_vpc.vpc.id
  count                   = length(split(",", var.private_subnets_cidr))
  cidr_block              = element(split(",", var.private_subnets_cidr), count.index)
  availability_zone       = element(split(",", var.azs), count.index)
  map_public_ip_on_launch = false

  tags= {
    Name = "${var.name}-${var.environment}-private-${element(split(",", var.azs), count.index)}"
  }
}

output "private_subnets_id" {
  value = join(",", aws_subnet.private.*.id)
}

// Create Private Route Table
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc.id

  tags= {
    Name = "${var.name}-private"
  }
}

resource "aws_route_table_association" "private" {
  count          = length(split(",", var.private_subnets_cidr))
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private.id
}


// Create Web Security Group

resource "aws_security_group" "web_sg" {
    name = "${var.name}-${var.environment}-web"
    description = "Security Group ${var.name}-${var.environment}"
    vpc_id = aws_vpc.vpc.id
    tags= {
      Name = "${var.name}-${var.environment}-web"
      environment =  var.environment
    }

    // allow traffic for TCP 80
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.websg_cidr_block]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }

    // allow traffic for TCP 8080
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [var.websg_cidr_block]
    }

    // allow traffic for TCP 443
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = [var.websg_cidr_block]
    }

        // allow traffic for ssh
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.websg_cidr_block]
    }

}

output "web_sg_id" {
  value = aws_security_group.web_sg.id
}

// Create DB Security Group

resource "aws_security_group" "rds_sg" {
    name = "${var.name}-${var.environment}-rds"
    description = "Security Group ${var.name}-${var.environment}"
    vpc_id = aws_vpc.vpc.id
    tags= {
      Name = "${var.name}-${var.environment}-rds"
      environment =  var.environment
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }

    // allows traffic from the SG itself
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        self = true
    }

    // allow traffic for TCP 5432
    ingress {
        from_port = 5432
        to_port = 5432
        protocol = "tcp"
        cidr_blocks = ["10.0.1.0/24","10.0.2.0/24","10.0.3.0/24"]

    }
}

output "rds_sg_id" {
  value = aws_security_group.rds_sg.id
}

// Create DB Subnet Group

resource "aws_db_subnet_group" "rds_subnet_group" {
  name = "${var.name}-${var.environment}-subnet-group"
  description = "Our main group of subnets"
  subnet_ids = aws_subnet.private.*.id
}

// Create DB instance

resource "aws_db_instance" "dbinstance" {
  allocated_storage    = 8
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "9.6.11"
  instance_class       = "db.t2.micro"
  name                 = "postgres"
  username             = "Ashok"
  password             = "Ashokashu12"
  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.id
  multi_az             = false
  publicly_accessible  = false
  vpc_security_group_ids = [aws_security_group.rds_sg.id] 

  tags= {
    environment = var.environment
  }
}

output "rds_address" {
  value = aws_db_instance.dbinstance.address
}

// Create EC2 instance

resource "aws_instance" "ec2" {
  ami = var.ami_id
  vpc_security_group_ids = [aws_security_group.web_sg.id]
  key_name = "IADTkp"
  instance_type = var.instance_type
  count = var.ec2count
  subnet_id = element(aws_subnet.public.*.id, count.index)
  iam_instance_profile = aws_iam_instance_profile.test_profile.name
  root_block_device {
        volume_type = var.ebs_root_volume_type
        volume_size = var.ebs_root_volume_size
        delete_on_termination = var.ebs_root_delete_on_termination
  }
  tags= {
    Name        = "${var.name}-${var.environment}"
    environment = var.environment
    server_role = var.server_role
  }
  user_data = <<-EOF
          #!/bin/bash
          aws s3 cp s3://iadt-project ~/iadt-project --recursive
          EOF
}

output "ec2_id" {
  value = "${join(",", aws_instance.ec2.*.id)}"
}


// IAM role and instance profile

resource "aws_iam_role" "test_role" {
  name = "test_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
    
  ]
}
EOF

  tags = {
      tag-key = "tag-value"
  }
}

resource "aws_iam_instance_profile" "test_profile" {
  name = "test_profile"
  role = aws_iam_role.test_role.name
}

resource "aws_iam_role_policy" "test_policy" {
  name = "test_policy"
  role = aws_iam_role.test_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
    
  ]
}
EOF
}
data "template_file" "user_data" {
  template = file("/home/ubuntu/terraform-aws/setup.sh")
  vars= {
    hostname = aws_db_instance.dbinstance.endpoint
  }
}

// Create launch configuration

resource "aws_launch_configuration" "web" {
  name_prefix = "web-iadt"

  image_id = var.ami_id
  instance_type = var.instance_type
  key_name = "IADTkp"

  security_groups = [aws_security_group.web_sg.id]
  associate_public_ip_address = true

  iam_instance_profile = aws_iam_instance_profile.test_profile.name
  user_data = data.template_file.user_data.rendered

  lifecycle {
    create_before_destroy = true
  }
}

// Create Load Balancer

resource "aws_security_group" "elb_http" {
  name        = "elb_http"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"
  vpc_id = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags ={
    Name = "Allow HTTP through ELB Security Group"
  }
}

# resource "aws_elb" "web_elb" {
#   name = "web-elb"
#   security_groups = [
#     "${aws_security_group.elb_http.id}"
#   ]
#   subnets = aws_subnet.public.*.id
#   cross_zone_load_balancing   = true
#   health_check {
#     healthy_threshold = 2
#     unhealthy_threshold = 2
#     timeout = 3
#     interval = 30
#     target = "HTTP:80/"
#   }
#   listener {
#     lb_port = 80
#     lb_protocol = "http"
#     instance_port = "80"
#     instance_protocol = "http"
#   }
# }

# output "ELB_IP" {
#   value = "${aws_elb.web_elb.dns_name}"
# }


// Create AutoScaling Group

resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-asg"

  min_size             = 1
  desired_capacity     = 2
  max_size             = 4

  # health_check_type    = "ELB"
  # load_balancers= [
  #   "${aws_elb.web_elb.id}"
  # ]

  launch_configuration = aws_launch_configuration.web.name
  #availability_zones = ["us-east-1a", "us-east-1b","us-east-1c"]

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity="1Minute"

  vpc_zone_identifier  = aws_subnet.public.*.id

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
     key                 = "Name"
     value               = "web"
     propagate_at_launch = true
   }
 }

#  resource "aws_autoscaling_attachment" "asg_attachment_bar" {
#   autoscaling_group_name = aws_autoscaling_group.web.id
#   elb                    = aws_elb.web_elb.id
# }

// policies and alarms for scaling up 

 resource "aws_autoscaling_policy" "web_policy_up" {
   name = "web_policy_up"
   scaling_adjustment = 1
   adjustment_type = "ChangeInCapacity"
   cooldown = 300
   autoscaling_group_name = aws_autoscaling_group.web.name
 }

resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_up" {
  alarm_name = "web_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "60"

  dimensions= {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = ["${aws_autoscaling_policy.web_policy_up.arn}"]
}

# resource "aws_autoscaling_policy" "web_policy_down" {
#   name = "web_policy_down"
#   scaling_adjustment = -1
#   adjustment_type = "ChangeInCapacity"
#   cooldown = 300
#   autoscaling_group_name = aws_autoscaling_group.web.name
# }

# resource "aws_cloudwatch_metric_alarm" "web_cpu_alarm_down" {
#   alarm_name = "web_cpu_alarm_down"
#   comparison_operator = "LessThanOrEqualToThreshold"
#   evaluation_periods = "2"
#   metric_name = "CPUUtilization"
#   namespace = "AWS/EC2"
#   period = "120"
#   statistic = "Average"
#   threshold = "10"

#   dimensions= {
#     AutoScalingGroupName = aws_autoscaling_group.web.name
#   }

#   alarm_description = "This metric monitor EC2 instance CPU utilization"
#   alarm_actions = ["${aws_autoscaling_policy.web_policy_down.arn}"]
# }

# resource "aws_codedeploy_app" "example" {
#   name = "iadt-app"
# }

# resource "aws_codedeploy_deployment_group" "example" {
#   app_name              = aws_codedeploy_app.example.name
#   deployment_group_name = "example-group"
#   service_role_arn      = "arn:aws:iam::843837413842:role/web_role"

#   deployment_style {
#     deployment_option = "WITH_TRAFFIC_CONTROL"
#     deployment_type   = "BLUE_GREEN"
#   }

#   load_balancer_info {
#     elb_info {
#       name = aws_elb.web_elb.name
#     }
#   }

#   blue_green_deployment_config {
#     deployment_ready_option {
#       action_on_timeout    = "STOP_DEPLOYMENT"
#       wait_time_in_minutes = 60
#     }

#     green_fleet_provisioning_option {
#       action = "DISCOVER_EXISTING"
#     }

#     terminate_blue_instances_on_deployment_success {
#       action = "KEEP_ALIVE"
#     }
#   }
# }

