variable "name" {
	default = "iadt"
}

variable "environment" {
  default = "project"
}

variable "enable_dns_support" {
  description = "should be true if you want to use private DNS within the VPC"
  default = true
}
variable "enable_dns_hostnames" {
  description = "should be true if you want to use private hostname within the VPC"
  default = true
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnets_cidr" {
	description = "CIDR for public subnets"
	default = "10.0.1.0/24,10.0.2.0/24,10.0.3.0/24"
}

variable "private_subnets_cidr" {
	description = "CIDR for private subnets"
	default = "10.0.4.0/24,10.0.5.0/24"
}

variable "azs" {
	description = "AZ for subnets"
    default = "us-east-1a,us-east-1b,us-east-1c"
}

variable "map_public_ip_on_launch" {
  default = true
}

variable "websg_cidr_block" {
  description = "The source CIDR block to allow traffic from for web SG"
  default = "0.0.0.0/0"
}

variable "server_role" {
  default = "test"
}

variable "instance_type" {
	description = "Type of instance"
	default = "t2.micro"
}

variable "user_data" {
	description = "User data that need to pass to the instance(s)"
}

variable "ami_id" {
  description = "AMI used to spin-up this EC2 Instance"
  default = "ami-0c3a24236a1349bcb"
}

variable "ec2count" {
  description = "Number of instance(s) to be launched"
  default = "1"
}

variable "ebs_root_volume_type" {
  default = "gp2"
}

variable "ebs_root_volume_size" {
  default = "8"
}

variable "ebs_root_delete_on_termination" {
  default = "true"
}
