#!/usr/bin/python3

import paramiko
from scp import SCPClient

ssh=paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

ssh.connect('3.89.66.208',
        username='ubuntu',
        key_filename='''IADTkp.pem''')

scp = SCPClient(ssh.get_transport())
scp.put('iadt_web_application', recursive=True, remote_path='/home/ubuntu/')


